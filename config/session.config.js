export default {
    user: {
        fio: "",
        address: "",
        isNewbie: true
    },
    routeHistory: [],
    order: {
        type: "",
        subType: "",
        name: "",
        link: "",
        params: "",
        price: "",
    },
    temp: {}
}