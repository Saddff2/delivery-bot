export default {
    boots: {
        factWeight: 1.32,
        sizes: [26, 36, 14],
    },
    sneakers: {
        factWeight: 0.85,
        sizes: [26, 36, 14],
    },
    slippers: {
        factWeight: 0.78,
        sizes: [26, 36, 14],
    },
    windbreaker: {
        factWeight: 0.75,
        sizes: [40, 29, 16],
    },
    overcoat: {
        factWeight: 1.03,
        sizes: [45, 34, 18],
    },
    coat: {
        factWeight: 1.45,
        sizes: [45, 34, 18],
    },
    down_jacket: {
        factWeight: 1.7,
        sizes: [45, 34, 18],
    },
    light_jacket: {
        factWeight: 1.02,
        sizes: [40, 29, 16],
    },
    t_shirt: {
        factWeight: 0.25,
        sizes: [23, 17, 13],
    },
    sweater: {
        factWeight: 0.52,
        sizes: [36, 26, 14],
    },
    hoodie: {
        factWeight: 0.67,
        sizes: [36, 26, 14],
    },
    turtleneck: {
        factWeight: 0.33,
        sizes: [23, 17, 13],
    },
    shirt: {
        factWeight: 0.34,
        sizes: [23, 17, 13],
    },
    jeans: {
        factWeight: 0.83,
        sizes: [36, 26, 14],
    },
    shorts: {
        factWeight: 0.4,
        sizes: [23, 17, 13],
    },
    trousers: {
        factWeight: 0.8,
        sizes: [23, 17, 13],
    },
    skirt: {
        factWeight: 0.65,
        sizes: [23, 17, 13],
    },
    fanny_pack: {
        factWeight: 0.47,
        sizes: [23, 17, 13],
    },
    travel_bag: {
        factWeight: 2.33,
        sizes: [45, 34, 18],
    },
    backpack: {
        factWeight: 1.72,
        sizes: [40, 29, 16],
    },
    satchel: {
        factWeight: 0.53,
        sizes: [23, 17, 13],
    },
    belt: {
        factWeight: 0.18,
        sizes: [23, 17, 13],
    },
    umbrella: {
        factWeight: 0.45,
        sizes: [23, 17, 13],
    },
    glasses: {
        factWeight: 0.13,
        sizes: [23, 17, 13],
    },
    wallet: {
        factWeight: 0.2,
        sizes: [23, 17, 13],
    },
    scarf: {
        factWeight: 0.28,
        sizes: [23, 17, 13],
    },
    gloves: {
        factWeight: 0.18,
        sizes: [23, 17, 13],
    },
};