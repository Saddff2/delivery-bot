export default {
    price: {
        min: 5,
        max: 20000
    },
    fio: {
        min: 4,
        max: 100,
    },
    address: {
        min: 10,
        max: 180,
    },
    maxOrdersPerMessage: 5,
};